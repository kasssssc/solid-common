import { HTTP, HTTPMethod, HTTPBlob, HttpError, HttpFormData, HttpArgs } from '#common/util/http';

export type HttpApiCall = {
  method: HTTPMethod;
  path: string;
  payload?: Record<string, any>;
  params?: Record<string, any>;
  headers?: Record<string, any>;
}

export const useHttpClient = (
  baseUrl: string,
  getJwt?: () => string | null,
  getHeaders?: Record<string, () => any>,
  errHandler?: (err: HttpError) => void,
) => {

  const resolveArgs = (args: HttpApiCall): HttpArgs => {
    const headers = { ...args.headers };
    for (const key in getHeaders) {
      headers[key] = getHeaders[key]();
    }

    return {
      method: args.method,
      url: baseUrl + args.path,
      payload: args.payload,
      params: args.params,
      headers: headers,
      ...(getJwt && { jwt: getJwt() }),
    };
  };

  const http = async <R>(args: HttpApiCall) => {
    const res = await HTTP<R>(resolveArgs(args));

    if (res?.error) {
      errHandler?.(res.error);
    }

    return res;
  };

  const httpFormData = async <R>(args: HttpApiCall) => {
    const res = await HttpFormData<R>(resolveArgs(args));

    if (res?.error) {
      errHandler?.(res.error);
    }

    return res;
  };

  const httpBlob = async (args: HttpApiCall) => {
    const res = await HTTPBlob(resolveArgs(args));

    if (res?.error) {
      errHandler?.(res as HttpError);
    }

    return res;
  };

  return {
    http,
    httpFormData,
    httpBlob,
  };
};
