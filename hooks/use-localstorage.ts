import { createEffect, createSignal } from 'solid-js';

const BoolToString = (bool: boolean): 'true' | 'false' => bool ? 'true' : 'false';
const StringToBool = (str?: string | null): boolean => str == 'true';

const LSGetBool = (key: string): boolean => StringToBool(localStorage.getItem(key));
const LSSetBool = (key: string, val: boolean) => localStorage.setItem(key, BoolToString(val));

export const useLocalStorageBool = (key: string, initialValue: boolean | null = null) => {
  const [get, set] = createSignal<boolean>(LSGetBool(key) ?? initialValue);

  createEffect(() => {
    LSSetBool(key, get());
  });

  return {
    get,
    set,
    remove: () => localStorage.removeItem(key),
  };
};

export const useLocalStorageStr = (key: string, initialValue: string | null = null) => {
  const [get, set] = createSignal<string|null>(localStorage.getItem(key) ?? initialValue);

  createEffect(() => {
    if (get() === null) {
      localStorage.removeItem(key);
    } else {
      localStorage.setItem(key, get() as string);
    }
  });

  return {
    get,
    set,
    remove: () => localStorage.removeItem(key),
  };
};
