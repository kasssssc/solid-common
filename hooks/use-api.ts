import { HttpError, HttpRes } from '#common/util/http';
import { createSignal } from 'solid-js';

export type ApiRes<T> = Promise<T & HttpError>;
// export type UnwrapPromise<T> = T extends Promise<infer U> ? U : T;

// export interface ApiHook<F extends (...args: any[]) => any> {
//   call: (...args: Parameters<F>) => Promise<Awaited<ReturnType<F>> & HttpError>;
//   loading: Accessor<boolean>;
//   error: Accessor<any>;
//   reset: () => void;
// }

export function useApi<R, A extends any[]>(
  apiFn: (...args: A) => Promise<HttpRes<R>>,
  onSuccess?: (res: R) => void,
  onError?: (err: HttpError) => void
) {
  const [loading, setLoading] = createSignal<boolean>(false);
  const [error, setError] = createSignal<HttpError>();
  const [active, setActive] = createSignal<boolean>(true);

  const call = async (...args: A) => {
    setLoading(true);
    setError(undefined);
    setActive(true);
    const res = await apiFn(...args);
    setLoading(false);
    if (!active()) return undefined;
    if (res?.error) {
      setError(res.error);
      onError?.(res.error);
    } else {
      onSuccess?.(res as R);
    }
    return res;
  };

  const reset = () => {
    setActive(false);
    setLoading(false);
    setError(undefined);
  };

  return {
    call,
    loading,
    error,
    reset,
  };
}
