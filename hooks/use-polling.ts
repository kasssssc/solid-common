import { createEffect, createSignal } from 'solid-js';

export const useManualPolling = (
  action: () => void,
  intervalMs: number,
  immediate: boolean = true,
) => {
  let interval: NodeJS.Timeout | undefined = undefined;

  const start = () => {
    if (!interval) {
      if (immediate) {
        action();
      }
      interval = setInterval(action, intervalMs);
    }
  };

  const stop = () => {
    if (interval) {
      clearInterval(interval);
      interval = undefined;
    }
  };

  const active = (): boolean => !!interval;

  return {
    start,
    stop,
    active,
  };
};

export const usePolling = (
  condition: () => boolean,
  action: () => void,
  intervalMs: number,
  autoStart?: boolean,
) => {
  const [active, setIsActive]  = createSignal<boolean>(!!autoStart);

  let interval: NodeJS.Timeout | undefined = undefined;

  const start = () => setIsActive(true);

  const stop = () => {
    if (interval) {
      clearInterval(interval);
      interval = undefined;
    }
    setIsActive(false);
  };

  createEffect(() => {
    if (interval && !condition()) {
      stop();
      return;
    }
    if (!interval && condition() && active()) {
      action();
      interval = setInterval(action, intervalMs);
    }
  });

  return {
    start,
    stop,
  };
};
