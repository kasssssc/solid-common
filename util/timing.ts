export const Defer = (fn: (...args: any[]) => void): (...args: any[]) => void => {
  return async (...args: any[]) => {
    await new Promise(resolve => {
      setTimeout(resolve, 0);
    });
    fn(...args);
  };
};

export const Debounce = <T extends (...args: any[]) => any>(
  fn: T,
  delay: number,
): (...args: Parameters<T>) => void => {
  let timeoutId: ReturnType<typeof setTimeout> | null;

  return (...args: Parameters<T>) => {
    clearTimeout(timeoutId!);

    timeoutId = setTimeout(() => {
      fn(...args);
    }, delay);
  };
};