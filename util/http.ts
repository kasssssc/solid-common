export enum HTTPMethod {
  Get = 'GET',
  Post = 'POST',
  Patch = 'PATCH',
  Put = 'PUT',
  Delete = 'DELETE',
}

export type HttpArgs = {
  method: HTTPMethod;
  url: string;
  payload?: Record<string, any>;
  params?: Record<string, any>;
  headers?: Record<string, string>;
  jwt?: string | null;
}

export type HttpError = {
  status: number;
  message: string;
  data?: any;
}

export type HttpRes<R> = R & { error?: HttpError };

export const HTTP = async <R>(args: HttpArgs): Promise<HttpRes<R>> => {
  const url = args.params
    ? `${args.url}?${new URLSearchParams(args.params)}`
    : args.url;
  const options = {
    method: args.method,
    headers: {
      'Content-Type': 'application/json',
      ...(args.jwt && { Authorization: `Bearer ${args.jwt}` }),
      ...args.headers,
    },
    body: JSON.stringify(args.payload),
  }; 
  const res = await fetch(url, options);
  const resJson = await res.json();
  if (!res.ok) {
    return {
      error: {
        ...resJson,
      },
    } as HttpRes<R>;
  }
  return resJson;
};

export const HttpFormData = async <R>(args: HttpArgs): Promise<HttpRes<R>> => {
  const url = args.params
    ? `${args.url}?${new URLSearchParams(args.params)}`
    : args.url;

  const formData = new FormData();
  if (args.payload) {
    for (const [key, value] of Object.entries(args.payload)) {
      if (Array.isArray(value)) {
        for (const v of value) {
          formData.append(key, v);
        }
      } else {
        formData.append(key, value);
      }
    }
  }
  
  const options = {
    method: args.method,
    headers: {
      ...(args.jwt && { Authorization: `Bearer ${args.jwt}` }),
      ...args.headers,
    },
    body: formData,
  };
  const res = await fetch(url, options);
  const resJson = await res.json();
  if (!res.ok) {
    return {
      error: {
        ...resJson,
      },
    } as HttpRes<R>;
  }
  return resJson;
};

export const HTTPBlob = async (args: HttpArgs): Promise<HttpRes<{data: Blob}>> => {
  const url = args.params
    ? `${args.url}?${new URLSearchParams(args.params)}`
    : args.url;
  const options = {
    method: args.method,
    headers: {
      'Content-Type': 'application/json',
      ...(args.jwt && { Authorization: `Bearer ${args.jwt}` }),
      ...args.headers,
    },
    body: JSON.stringify(args.payload),
  };
  const res = await fetch(url, options);
  if (!res.ok) {
    const resJson = await res.json();
    return {
      error: {
        ...resJson,
      },
    } as HttpRes<{data: Blob}>;
  }

  const blob = await res.blob();
  return { data: blob };
};
