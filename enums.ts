export enum Keyboard {
  Space = ' ',
  Tab = 'Tab',
  Enter = 'Enter',
  Esc = 'Escape',
  Up = 'ArrowUp',
  Down = 'ArrowDown',
  Left = 'ArrowLeft',
  Right = 'ArrowRight',
  Backspace = 'Backspace',
  Comma = ',',
}

export enum Size {
  SM = 'sm',
  MD = 'md',
  LG = 'lg',
}