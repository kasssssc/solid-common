import {
  Component,
  Show,
  For,
  createSignal,
  createEffect,
} from 'solid-js';

import {
  SelectOption,
  CreateInternalOptions,
  useSingleSelect,
  useMultipleSelect,
} from './select-utils';
import { Dropdown } from '../layout';
import { XBtn } from '../buttons';

import { Keyboard } from '../../enums';
import { Defer, Debounce } from '../../util';
import clsx from 'clsx';

interface SelectProps {
  options: any[];
  disabled?: boolean;
  searchable?: boolean;
  multiple?: boolean;
  placeholder?: string;
  clearable?: boolean;
  value?: string | string[];
  resolveDisplay?: (o: any) => string;
  resolveValue?: (o: any) => any;
  onchange?: (selection?: string | string[]) => void;
  size?: string;
  class?: string;
  dropdownClass?: string;
}

export const Select: Component<SelectProps> = (props) => {

  const [open, setOpen] = createSignal<boolean>(false);
  const [filter, setFilter] = createSignal<string>('');
  const [internalOptions, setInternalOptions] = createSignal<SelectOption[]>(
    CreateInternalOptions(props.options, props.resolveDisplay, props.resolveValue),
  );
  const debouncedSetFilter = Debounce(setFilter, 100);

  const state = props.multiple
    ? useMultipleSelect(props.value as string[])
    : useSingleSelect(props.value as string);

  let selectWrapperEl: HTMLDivElement;
  let selectTriggerEl: HTMLDivElement;
  let selectTextInputEl: HTMLInputElement;

  const textInputDisplay = (): string => {
    if (open() && props.searchable) {
      return filter();
    }
    if (props.multiple) {
      return '';
    }
    const selectedValue = state.getSelectionValue();
    return internalOptions().find(o => selectedValue === o.value)?.display || '';
  };

  const getPlaceholder = (): string => {
    if (open() && props.searchable) {
      return 'Type to search...';
    }
    if (props.multiple) {
      return state.hasSelection()
        ? ''
        : props.placeholder || 'Select multiple';
    }
    return props.placeholder || 'Select';
  };

  const onOptionClick = (_: MouseEvent, option: SelectOption) => {
    state.onOptionSelect(option);
    if (!props.multiple) {
      setOpen(false);
    }
    props.onchange?.(state.getSelectionValue());
  };

  const dropdownOnKeyDown = (
    $ev: KeyboardEvent,
    highlightedOption: SelectOption,
  ) => {
    switch ($ev.key) {
    case Keyboard.Enter:
      if (highlightedOption && !props.multiple) {
        state.onOptionSelect(highlightedOption);
        props.onchange?.(state.getSelectionValue());
      }
      setOpen(false);
      break;
    case Keyboard.Space:
      $ev.preventDefault();
      if (highlightedOption) {
        state.onOptionSelect(highlightedOption);
        props.onchange?.(state.getSelectionValue());
      }
      break;
    case Keyboard.Esc:
    case Keyboard.Tab:
      setOpen(false);
      break;
    default: break;
    }
  };

  const clickOutsideListener = (ev: MouseEvent) => {
    if (!selectWrapperEl.contains(ev.target as Node)) {
      setOpen(false);
    }
  };

  createEffect(() => {
    if (open()) {
      document.addEventListener('click', clickOutsideListener);
      if (props.searchable) {
        selectTextInputEl.focus();  
      }
    } else {
      document.removeEventListener('click', clickOutsideListener);
      if (props.searchable) {
        setFilter('');
        selectTextInputEl.blur();
      } else {
        selectTriggerEl.blur();
      }
    }
  });

  createEffect(() => {
    state.setSelectionValue(props.value as any);
  });

  createEffect(() => {
    setInternalOptions(CreateInternalOptions(props.options, props.resolveDisplay, props.resolveValue));
  });

  return (
    <div
      class={clsx(
        '-select-wrapper',
        props.size,
        props.class,
        props.disabled && 'disabled',
      )}
      ref={selectWrapperEl!}
      data-open={open()}
      data-searching={!!filter()}
      data-has-value={state.hasSelection()}
    >
      <div
        class="-select-trigger"
        ref={selectTriggerEl!}
        onclick={() => setOpen(!open())}
        tabIndex={props.searchable ? -1 : 0}
      />
      <Show when={props.multiple}>
        <MultipleSelectTags
          options={internalOptions()}
          selection={state.selection() as Set<string>}
          onTagClick={Defer((o: SelectOption) => {
            state.onOptionSelect(o);
            props.onchange?.(state.getSelectionValue());
          })}
        />
      </Show>
      <input
        class="-select-text-input"
        ref={selectTextInputEl!}
        disabled={!props.searchable}
        placeholder={getPlaceholder()}
        value={textInputDisplay()}
        onfocus={() => setOpen(true)}
        oninput={($ev) => debouncedSetFilter($ev.currentTarget.value)}
      />
      <Show when={props.clearable && state.hasSelection()}>
        <XBtn onclick={Defer(() => {
          state.clearSelection();
          props.onchange?.(state.getSelectionValue());
        })} />
      </Show>
      <div class="-select-arrow" onclick={() => setOpen(!open())}>
        <i class="ri-arrow-down-s-line" />
      </div>
      <Dropdown
        class={props.dropdownClass}
        open={open()}
        filter={filter()}
        options={internalOptions()}
        isSelected={state.isSelected}
        onKeyDown={dropdownOnKeyDown}
        onOptionClick={onOptionClick}
      />
    </div>
  );
};

interface MultipleSelectTagsProps {
  options: SelectOption[]
  selection?: Set<string>;
  onTagClick: (o: SelectOption) => void;
}
const MultipleSelectTags: Component<MultipleSelectTagsProps> = (props) => {
  return (
    <div class="-select-tag-container">
      <Show when={props.selection}>
        <For each={props.options.filter(o => props.selection!.has(o.value))}>
          { (selectedOption) => (
            <div class="-select-tag">
              {selectedOption.display}
              <XBtn onclick={() => props.onTagClick(selectedOption)} /> 
            </div>
          )}
        </For>
      </Show>
    </div>
  );
};
