import { JSX, Setter, Component, Show, splitProps, createSignal } from 'solid-js';
import clsx from 'clsx';

import { Button, XBtn } from '../buttons';

interface InputProps extends JSX.HTMLAttributes<HTMLInputElement> {
  value?: string | number;
  setter?: Setter<string>;
  color?: string;
  size?: 'sm' | 'md' | 'lg';
  wrapperClass?: string;
  wrapperClassList?: Record<string, boolean>;
  clearable?: boolean;
  onClear?: () => void;
  icon?: string;
  type?: string;
  disabled?: boolean;
  placeholder?: string;
  maxLength?: number;
  autocomplete?: string;
  lazy?: boolean;
  oninput?: JSX.EventHandler<HTMLInputElement, InputEvent>;
}

export const Input: Component<InputProps> = (props) => {
  const [_, nativeProps] = splitProps(
    props,
    [
      'oninput', 'setter',
      'size', 'icon', 'color', 'clearable',
      'wrapperClass', 'wrapperClassList', 'class',
    ],
  );

  const onInput: JSX.EventHandler<HTMLInputElement, InputEvent> = ($ev) => {
    props.setter?.($ev.currentTarget.value);
    props.oninput?.($ev);
  };

  const onClear = () => {
    props.setter?.('');
    props.onClear?.();
  };

  const [showPassword, setShowPassword] = createSignal(false);
  const togglePasswordVisibility = () => {
    setShowPassword(!showPassword());
  };

  return (
    <div
      class={clsx('-input-wrapper', props.wrapperClass)}
      classList={props.wrapperClassList}
      data-clearable={!!props.clearable}
      data-has-icon={!!props.icon}
    >
      <input
        class={clsx(
          '-input-control',
          props.size,
          props.color,
          props.class
        )}
        oninput={($ev) => onInput($ev)}
        {...nativeProps}
        type={showPassword() ? "text" : props.type}
      />
      <Show when={props.type === 'password'}>
        <Button
          onClick={togglePasswordVisibility}
          class='absolute right-3 top-1/2 transform -translate-y-1/2 bg-transparent border-none cursor-pointer'
        >
          {showPassword() ? (
            <svg class='w-6' viewBox="0 0 33 21" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M16.0302 15.3011C18.7561 15.3011 20.9659 13.0913 20.9659 10.3654C20.9659 7.63948 18.7561 5.42969 16.0302 5.42969C13.3043 5.42969 11.0945 7.63948 11.0945 10.3654C11.0945 13.0913 13.3043 15.3011 16.0302 15.3011Z" fill="#7E7D7D" />
              <path d="M31.5684 8.99781C27.7744 4.41315 22.0314 0 16.0301 0C10.0277 0 4.28331 4.41623 0.491875 8.99781C-0.163958 9.78999 -0.163958 10.94 0.491875 11.7322C1.44509 12.8841 3.44343 15.1076 6.11242 17.0504C12.8342 21.9437 19.2113 21.9545 25.9478 17.0504C28.6168 15.1076 30.6152 12.8841 31.5684 11.7322C32.2223 10.9416 32.2257 9.79259 31.5684 8.99781ZM16.0301 3.455C19.8405 3.455 22.9401 6.55463 22.9401 10.365C22.9401 14.1754 19.8405 17.275 16.0301 17.275C12.2198 17.275 9.12012 14.1754 9.12012 10.365C9.12012 6.55463 12.2198 3.455 16.0301 3.455Z" fill="#7E7D7D" />
            </svg>
          ) : (
            <svg class='w-6' viewBox="0 0 28 20" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M22.9537 18.2137L5.0454 0.305942L5.04157 0.301567C4.94462 0.205403 4.82967 0.129277 4.7033 0.0775352C4.57692 0.0257938 4.44159 -0.000549911 4.30504 8.70113e-06C4.16848 0.000567313 4.03338 0.0280174 3.90743 0.0807909C3.78148 0.133564 3.66716 0.210628 3.571 0.307583C3.47483 0.404537 3.39871 0.519482 3.34697 0.645857C3.29523 0.772231 3.26888 0.907559 3.26944 1.04411C3.27057 1.3199 3.38121 1.58394 3.57701 1.77816L5.14493 3.34607C3.24348 4.64834 1.60679 6.30006 0.321957 8.21335C0.112145 8.52312 0 8.88868 0 9.26282C0 9.63696 0.112145 10.0025 0.321957 10.3123C3.60162 15.1664 8.80086 18.0102 14.0001 17.9747C15.6842 17.9808 17.3571 17.7004 18.9472 17.1456L21.4853 19.6837C21.6809 19.8729 21.9428 19.9779 22.215 19.9761C22.4871 19.9742 22.7476 19.8657 22.9406 19.6738C23.1335 19.4819 23.2435 19.222 23.2469 18.9499C23.2503 18.6778 23.1467 18.4152 22.9586 18.2186L22.9537 18.2137ZM14.0001 15.1522C13.015 15.1519 12.0458 14.9046 11.1809 14.4331C10.316 13.9616 9.58311 13.2809 9.04913 12.4531C8.51515 11.6254 8.19713 10.677 8.12415 9.69463C8.05116 8.71229 8.22553 7.72731 8.63132 6.82973L10.2692 8.46765C10.1373 9.08934 10.1628 9.73415 10.3434 10.3435C10.524 10.9528 10.854 11.5074 11.3034 11.9568C11.7528 12.4062 12.3073 12.7362 12.9167 12.9168C13.526 13.0974 14.1708 13.1229 14.7925 12.9909L16.4304 14.6289C15.667 14.975 14.8383 15.1535 14.0001 15.1522Z" fill="#7E7D7D" />
              <path d="M27.6782 8.21136C24.3986 3.35448 19.1993 0.51068 14.0001 0.546228C12.316 0.540128 10.6431 0.820484 9.05298 1.3753L11.5686 3.89097C12.6603 3.39696 13.8765 3.24737 15.0553 3.46214C16.2341 3.67691 17.3195 4.24583 18.1667 5.09309C19.014 5.94035 19.5829 7.0257 19.7977 8.2045C20.0125 9.3833 19.8629 10.5996 19.3689 11.6912L22.8547 15.177C24.7565 13.875 26.3934 12.2232 27.6782 10.3098C27.8879 10 28 9.63459 28 9.26056C28 8.88653 27.8879 8.52108 27.6782 8.21136Z" fill="#7E7D7D" />
              <path d="M14.0002 5.44629C13.7339 5.44632 13.4683 5.47418 13.2078 5.52942L17.731 10.0522C17.849 9.49647 17.8413 8.92145 17.7086 8.36911C17.5759 7.81676 17.3214 7.30105 16.9639 6.85963C16.6063 6.41821 16.1547 6.06223 15.6419 5.81771C15.1292 5.57319 14.5683 5.44629 14.0002 5.44629Z" fill="#7E7D7D" />
            </svg>
          )}
        </Button>
      </Show>
      <Show when={props.icon}>
        <div class="-input-icon">
          <i class={props.icon} />
        </div>
      </Show>
      <Show when={props.clearable && props.value}>
        <XBtn onclick={() => onClear()} />
      </Show>
    </div>
  );
};
