import { JSX, Setter, Component, Show, splitProps } from 'solid-js';
import clsx from 'clsx';

import { XBtn } from '../buttons';

interface TextAreaProps extends JSX.HTMLAttributes<HTMLTextAreaElement> {
  value?: string;
  setter?: Setter<string>;
  color?: string;
  size?: 'sm' | 'md' | 'lg';
  wrapperClass?: string;
  wrapperClassList?: Record<string, boolean>;
  clearable?: boolean;
  icon?: string;
  type?: string;
  disabled?: boolean;
  placeholder?: string;
  oninput?: JSX.EventHandler<HTMLTextAreaElement, InputEvent>;
  rows?: string;
}

export const TextArea: Component<TextAreaProps> = (props) => {
  const [_, nativeProps] = splitProps(
    props,
    [
      'oninput', 'setter',
      'size', 'icon', 'color', 'clearable',
      'wrapperClass', 'wrapperClassList', 'class',
    ],
  );

  const onInput: JSX.EventHandler<HTMLTextAreaElement, InputEvent> = ($ev) => {
    props.setter?.($ev.currentTarget.value);
    props.oninput?.($ev);
  };

  return (
    <div
      class={clsx('-input-wrapper', props.wrapperClass)}
      classList={props.wrapperClassList}
      data-clearable={!!props.clearable}
      data-has-icon={!!props.icon}
    >
      <textarea
        class={clsx(
          '-input-control',
          props.size,
          props.color,
          props.class
        )}
        oninput={onInput}
        {...nativeProps}
      />
      <Show when={props.icon}>
        <div class="-input-icon">
          <i class={props.icon} />
        </div>
      </Show>
      <Show when={props.clearable && props.value}>
        <XBtn onclick={() => props.setter?.('')} />
      </Show>
    </div>
  );
};
