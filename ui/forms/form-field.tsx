import { ParentComponent, Show, mergeProps } from 'solid-js';

interface FormFieldProps {
  label?: string;
  layout?: 'vertical' | 'horizontal';
  wrapperClass?: string;
  wrapperClassList?: Record<string, boolean>;
  labelClass?: string;
  labelClassList?: Record<string, boolean>;
}

export const FormField: ParentComponent<FormFieldProps> = (props) => {
  const merged = mergeProps({
    layout: 'vertical',
    wrapperClass: '',
    labelClass: '',
  }, props);

  return (
    <div
      data-layout={merged.layout}
      class={`-form-field ${merged.wrapperClass}`}
      classList={props.wrapperClassList}
    >
      <Show when={props.label}>
        <label
          class={merged.labelClass}
          classList={props.labelClassList}
        >
          { props.label }
        </label>
      </Show>
      { props.children }
    </div>
  );
};