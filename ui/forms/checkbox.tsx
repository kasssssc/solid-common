import { Show, type Component } from 'solid-js';
import { Checkbox as KbCheckbox } from '@kobalte/core';
import clsx from 'clsx';

interface CheckboxProps {
  class?: string;
  checked: boolean;
  onchange: (checked: boolean) => void; 
  label?: string;
  disabled?: boolean;
  description?: string;
  color?: string;
  disabledMark?: boolean;
}
export const Checkbox: Component<CheckboxProps> = (props) => {
  return (
    <KbCheckbox.Root
      class={clsx(
        '-checkbox-container',
        props.disabled && 'disabled',
        props.class,
      )}
      checked={props.checked}
      onChange={props.onchange}
    >
      <KbCheckbox.Input class="-checkbox-input" />
      <KbCheckbox.Control class={clsx('-checkbox-control', props.color)}>
        <Show when={props.disabledMark && props.disabled}>
          <i class="ri-close-line text-red" />
        </Show>
        <KbCheckbox.Indicator>
          <i class="ri-check-line" />
        </KbCheckbox.Indicator>
      </KbCheckbox.Control>
      { props.label && (
        <KbCheckbox.Label class="-checkbox-label">{ props.label }</KbCheckbox.Label>
      )}
      { props.description && (
        <KbCheckbox.Description class="-checkbox-description">{ props.description }</KbCheckbox.Description>
      )}
    </KbCheckbox.Root>
  );
};
