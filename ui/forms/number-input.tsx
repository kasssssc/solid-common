import { JSX, Component, Show, splitProps, createSignal, createEffect } from 'solid-js';
import clsx from 'clsx';

interface NumberInputProps extends JSX.HTMLAttributes<HTMLInputElement> {
  value?: number;
  valueChange: (num?: number) => void;
  color?: string;
  size?: 'sm' | 'md' | 'lg';
  wrapperClass?: string;
  wrapperClassList?: Record<string, boolean>;
  icon?: string;
  disabled?: boolean;
  placeholder?: string;
  min?: number;
  max?: number;
  required?: boolean;
  decimalPlaces?: number;
  selectOnFocus?: boolean;
}

export const NumberInput: Component<NumberInputProps> = (props) => {
  const [_, nativeProps] = splitProps(
    props,
    [
      'size', 'icon', 'color',
      'wrapperClass', 'wrapperClassList', 'class',
    ],
  );

  const [display, setDisplay] = createSignal<string>();

  const value = (num?: number): string => {
    if (!num && num !== 0) {
      return '';
    }

    if (num === 0) {
      return '0';
    }

    if (props.decimalPlaces) {
      return num.toFixed(props.decimalPlaces);
    }

    return num.toString();
  };

  const onChange = (raw: string) => {
    setDisplay('');

    if (raw === '' && !props.required) {
      props.valueChange(undefined);
      return;
    }

    let num = props.decimalPlaces ? parseFloat(raw): parseInt(raw);
    
    if (isNaN(num)) {
      setDisplay(value(0));
      props.valueChange(0);
      return;
    }

    if (props.min !== undefined && num < props.min) {
      setDisplay(value(props.min));
      props.valueChange(props.min);
      return;
    }

    if (props.max !== undefined && num > props.max) {
      setDisplay(value(props.max));
      props.valueChange(props.max);
      return;
    }

    // round decimal places
    if (props.decimalPlaces) {
      num = Number(num.toFixed(props.decimalPlaces));
    }

    setDisplay(value(num));
    props.valueChange(num);
  };

  const onFocus: JSX.EventHandler<HTMLInputElement, FocusEvent> = ($ev) => {
    if (props.selectOnFocus) {
      $ev.currentTarget.select();
    }
  };

  createEffect(() => {
    setDisplay(value(props.value));
  });

  return (
    <div
      class={clsx('-input-wrapper', props.wrapperClass)}
      classList={props.wrapperClassList}
      data-has-icon={!!props.icon}
    >
      <input
        class={clsx(
          '-input-control',
          props.size,
          props.color,
          props.class
        )}
        {...nativeProps}
        value={display()}
        onfocus={onFocus}
        onchange={($ev) => onChange($ev.target.value)}
      />
      <Show when={props.icon}>
        <div class="-input-icon">
          <i class={props.icon} />
        </div>
      </Show>
    </div>
  );
};
