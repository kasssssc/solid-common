import { Accessor, createSignal } from 'solid-js';
import { createStore } from 'solid-js/store';

export interface SelectOption {
  display: string;
  value: any;
}

/**
 * Resolve provided options array to internal representation
 * Value Priority: resolveDisplay fn -> option.value -> raw option
 * Display Priority: resolveValue fn -> option.display -> raw option
*/ 
export const CreateInternalOptions = (
  options: any[],
  resolveDisplay?: (o: any) => string,
  resolveValue?: (o: any) => any,
): SelectOption[] => {
  return options.map(o => ({
    display: resolveDisplay?.(o) ?? o.display ?? o,
    value: resolveValue?.(o) ?? o.value ?? o,
  }));
};

interface SelectionState<T> {
  selection: Accessor<string|Set<string>|undefined>;
  getSelectionValue: () => T | undefined;
  hasSelection: () => boolean;
  isSelected: (option: SelectOption) => boolean;
  onOptionSelect: (option: SelectOption) => void;
  setSelectionValue: (value?: T) => void;
  clearSelection: () => void;
}

export const useSingleSelect = (initial?: string): SelectionState<string> => {
  const [selection, setSelection] = createSignal<string|undefined>(initial);

  const getSelectionValue = () => selection();
  const setSelectionValue = (value?: string) => setSelection(value);
  const hasSelection = () => !!selection();
  const isSelected = (option: SelectOption) => option.value === selection();
  const onOptionSelect = (option: SelectOption) => setSelection(option.value);
  const clearSelection = () => setSelection(undefined);

  return {
    selection,
    getSelectionValue,
    setSelectionValue,
    hasSelection,
    isSelected,
    onOptionSelect,
    clearSelection,
  };
};
// export const useSingleSelect = (initial?: string): SelectionState => {
//   console.log(initial);
//   const [selection, setSelection] = createStore<any>({
//     value: initial,
//     get selectionValue() {
//       return this.selection;
//     },
//     get hasSelection() {
//       return !!this.selection;
//     }
//   });

//   const setSelectionValue = (value: any) => setSelection("value", value);
//   const isSelected = (option: SelectOption) => option.value === selection.selection;
//   const onOptionSelect = (option: SelectOption) => setSelection("value", option.value);
//   const clearSelection = () => setSelection("value", undefined);

//   return {
//     selection,
//     setSelectionValue,
//     isSelected,
//     onOptionSelect,
//     clearSelection,
//   }
// };

export const useMultipleSelect = (initial?: string[]): SelectionState<string[]> => {
  const [selection, setSelection] = createSignal<Set<string>>(
    new Set(initial ? [...initial] : []),
  );

  const getSelectionValue = () => [...selection()];
  const setSelectionValue = (value?: string[]) => setSelection(new Set(value ? [...value] : []));
  const hasSelection = () => selection().size > 0;
  const isSelected = (option: SelectOption) => selection().has(option.value);
  const onOptionSelect = (option: SelectOption) => {
    setSelection((prev) => {
      const newSet = new Set([...prev]);
      if (prev.has(option.value)) {
        newSet.delete(option.value);
      } else {
        newSet.add(option.value);
      }
      return newSet;
    });
  };
  const clearSelection = () => setSelection(new Set<string>());

  return {
    selection,
    getSelectionValue,
    setSelectionValue,
    hasSelection,
    isSelected,
    onOptionSelect,
    clearSelection,
  };
};