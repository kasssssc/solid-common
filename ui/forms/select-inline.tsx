import {
  Component,
  For,
  createSignal,
  createEffect,
} from 'solid-js';
import clsx from 'clsx';

import {
  SelectOption,
  CreateInternalOptions,
  useSingleSelect,
  useMultipleSelect,
} from './select-utils';
import { Button } from '../buttons';

interface SelectInlineProps {
  options: any[];
  multiple?: boolean;
  value?: string | string[];
  btnClass?: string;
  resolveDisplay?: (o: any) => string;
  resolveValue?: (o: any) => any;
  onchange?: (selection: string | string[]) => void;
  onselect?: (selection: string, selected: boolean) => void;
}

export const SelectInline: Component<SelectInlineProps> = (props) => {

  const [internalOptions, setInternalOptions] = createSignal<SelectOption[]>([]);

  const state = props.multiple
    ? useMultipleSelect()
    : useSingleSelect();

  const onOptionClick = (_: MouseEvent, option: SelectOption) => {
    state.onOptionSelect(option);
    props.onselect?.(option.value, state.isSelected(option));
    props.onchange?.(state.getSelectionValue() as any);
  };

  createEffect(() => {
    state.setSelectionValue(props.value as any);
  });

  createEffect(() => {
    setInternalOptions(CreateInternalOptions(props.options));
  });

  return (
    <div class="-select-inline-wrapper">
      <For each={internalOptions()}>
        { option => (
          <Button
            class={clsx('-select-inline-btn', props.btnClass)}
            data-selected={state.isSelected(option)}
            disabled={!props.multiple && state.isSelected(option)}
            text={option.display}
            onclick={$ev => onOptionClick($ev, option)}
          />
        )}
      </For>
    </div>
  );
};
