import { Component,/* , Show, For */ 
createEffect,
createSignal,
JSX} from 'solid-js';
import { Input } from './input';
import clsx from 'clsx';
import { Modal } from '../layout';
// import {
//   DatePicker,
//   DatePickerControl,
//   DatePickerInput,
//   DatePickerTrigger,
//   DatePickerClearTrigger,
//   DatePickerContent,
//   DatePickerYearSelect,
//   DatePickerMonthSelect,
//   DatePickerPrevTrigger,
//   DatePickerViewTrigger,
//   DatePickerNextTrigger,
//   DatePickerGrid,
//   DatePickerRowHeader,
//   DatePickerColumnHeader,
//   DatePickerRowGroup,
//   DatePickerRow,
//   DatePickerDayCell,
//   DatePickerDayCellTrigger,
//   DatePickerMonthCell,
//   DatePickerMonthCellTrigger,
//   DatePickerYearCell,
//   DatePickerYearCellTrigger,
// } from '@ark-ui/solid';
// import { Button } from '../buttons';

const IsValidDate = (day: number, month: number, year: number): boolean =>  {
  // JavaScript's Date object uses 0-based indexing for months (0 = January, 11 = December)
  const date = new Date(Date.UTC(year, month, day, 0, 0, 0));

  // Check if the resulting date matches the input values
  return (
    date.getFullYear() === year &&
    date.getMonth() === month &&
    date.getDate() === day
  );
}

const ParseDate = (val: string): Date => {
  const dateParts = val.split('/');

  const day = parseInt(dateParts[0], 10);
  const month = parseInt(dateParts[1], 10) - 1; // JavaScript months are 0-based (0 = January, 1 = February, ...)
  let year = parseInt(dateParts[2], 10);

  if (year < 1000) {
    year += 2000;
  }

  if (IsValidDate(day, month, year)) {
    return new Date(Date.UTC(year, month, day, 0, 0, 0));
  }

  return new Date('invalid date');
};

interface DateInputProps {
  value?: Date;
  valueChange?: (x?: Date) => void;
  class?: string;
  wrapperClass?: string;
  size?: 'sm' | 'md' | 'lg';
  disabled?: boolean;
  selectOnFocus?: boolean;
}
export const DateInput: Component<DateInputProps> = (props) => {
  const [display, setDisplay] = createSignal<string>('');
  const [invalidModal, setInvalidModal] = createSignal<boolean>(false);
  const [invalidInput, setInvalidInput] = createSignal<string>();

  const onInputChange = (value: string) => {
    const date = ParseDate(value);
    
    if (date.toString() !== 'Invalid Date') {
      props.valueChange?.(date);
    } else {
      setInvalidInput(value);
      setInvalidModal(true);
      props.valueChange?.(undefined);
      setDisplay('');
    }
  };

  const onFocus: JSX.EventHandler<HTMLInputElement, FocusEvent> = ($ev) => {
    if (props.selectOnFocus) {
      $ev.currentTarget.select();
    }
  };

  createEffect(() => {
    setDisplay(props.value ? props.value.toLocaleDateString('en-GB') : '');
  });

  return (<>
    <div class={clsx('-input-wrapper', props.wrapperClass)}>
      <input
        class={clsx(
          '-input-control',
          props.size,
          props.class
        )}
        placeholder="dd/mm/yyyy"
        disabled={props.disabled}
        value={display()}
        onfocus={onFocus}
        onchange={($ev) => onInputChange($ev.target.value)}
        oninput={($ev) => setDisplay($ev.target.value)}
      />
    </div>

    <Modal
      open={invalidModal()}
      close={() => {
        setInvalidModal(false);
        setInvalidInput();
      }}
      size="sm"
      content={() => (
        <div class="w-full flex flex-col items-center gap-3">
          <h2 class="text-red">
            Invalid Date!
          </h2>
          <h6 class="text-center">
            Please enter a valid date in the format dd/mm/YYYY
          </h6>
          <h6>
            You entered: {invalidInput()}
          </h6>
        </div>
      )}
    />
 </>);
  // return (
  //   <DatePicker locale="en-GB" value={props.value} onChange={(x) => props.onchange(x)}>
  //     {(api) => (
  //       <div class="-date-input-wrapper">
  //         <DatePickerControl class="-date-input-control">
  //           <DatePickerInput class="-input-control" placeholder="dd/mm/yyyy" />
  //           <DatePickerTrigger class="-date-input-trigger">
  //             <Button class="secondary" icon="ri-calendar-line" />
  //           </DatePickerTrigger>
  //           {/* <DatePickerClearTrigger>Clear</DatePickerClearTrigger> */}
  //         </DatePickerControl>
  //         <DatePickerContent class="-date-input-calendar-wrapper">
  //           {/* <DatePickerYearSelect />
  //           <DatePickerMonthSelect /> */}
  //           <div class="-date-picker-nav">
  //             <DatePickerPrevTrigger>
  //               <Button class="secondary" icon="ri-arrow-left-s-line" />
  //             </DatePickerPrevTrigger>
  //             <DatePickerViewTrigger class="-btn secondary">
  //               <Show when={api().view === 'day'}>
  //                 {api().visibleRangeText.start}
  //               </Show>
  //               <Show when={api().view === 'month'}>
  //                 {api().visibleRange.start.year}
  //               </Show>
  //               <Show when={api().view === 'year'}>
  //                 {api().getDecade().start} - {api().getDecade().end}
  //               </Show>
  //             </DatePickerViewTrigger>
  //             <DatePickerNextTrigger>
  //               <Button class="secondary" icon="ri-arrow-right-s-line" />
  //             </DatePickerNextTrigger>
  //           </div>
  //           <Show when={api().view === 'day'}>
  //             <DatePickerGrid class="-date-picker-grid">
  //               <DatePickerRowHeader class="-date-picker-day-row -date-picker-row-header">
  //                 <For each={api().weekDays}>
  //                   {(day) => (
  //                     <DatePickerColumnHeader aria-label={day.long} class="-date-picker-cell">
  //                       {day.narrow}
  //                     </DatePickerColumnHeader>
  //                   )}
  //                 </For>
  //               </DatePickerRowHeader>
  //               <DatePickerRowGroup>
  //                 <For each={api().weeks}>
  //                   {(week) => (
  //                     <DatePickerRow class="-date-picker-day-row">
  //                       <For each={week}>
  //                         {(day) => (
  //                           <DatePickerDayCell value={day} class="-date-picker-cell">
  //                             <DatePickerDayCellTrigger class="-date-picker-btn">
  //                               {day.day}
  //                             </DatePickerDayCellTrigger>
  //                           </DatePickerDayCell>
  //                         )}
  //                       </For>
  //                     </DatePickerRow>
  //                   )}
  //                 </For>
  //               </DatePickerRowGroup>
  //             </DatePickerGrid>
  //           </Show>
  //           <Show when={api().view === 'month'}>
  //             <DatePickerGrid class="-date-picker-grid">
  //               <DatePickerRowGroup>
  //                 <For each={api().getMonthsGrid({ columns: 4, format: 'short' })}>
  //                   {(months) => (
  //                     <DatePickerRow class="-date-picker-row">
  //                       <For each={months}>
  //                         {(month) => (
  //                           <DatePickerMonthCell value={month.value} class="-date-picker-cell">
  //                             <DatePickerMonthCellTrigger class="-date-picker-btn">
  //                               {month.label}
  //                             </DatePickerMonthCellTrigger>
  //                           </DatePickerMonthCell>
  //                         )}
  //                       </For>
  //                     </DatePickerRow>
  //                   )}
  //                 </For>
  //               </DatePickerRowGroup>
  //             </DatePickerGrid>
  //           </Show>
  //           <Show when={api().view === 'year'}>
  //             <DatePickerGrid class="-date-picker-grid">
  //               <DatePickerRowGroup>
  //                 <For each={api().getYearsGrid({ columns: 4 })}>
  //                   {(years) => (
  //                     <DatePickerRow class="-date-picker-row">
  //                       <For each={years}>
  //                         {(year) => (
  //                           <DatePickerYearCell value={year.value} class="-date-picker-cell">
  //                             <DatePickerYearCellTrigger class="-date-picker-btn">
  //                               {year.label}
  //                             </DatePickerYearCellTrigger>
  //                           </DatePickerYearCell>
  //                         )}
  //                       </For>
  //                     </DatePickerRow>
  //                   )}
  //                 </For>
  //               </DatePickerRowGroup>
  //             </DatePickerGrid>
  //           </Show>
  //         </DatePickerContent>
  //       </div>
  //     )}
  //   </DatePicker>
  // );
};