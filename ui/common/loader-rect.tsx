import { mergeProps } from 'solid-js';

export const LoaderRect = (props: {
  size?: string;
  color?: string;
  loaderClass?: string;
}) => {
  const merged = mergeProps({
    size: '',
    color: '',
    loaderClass: '',
  }, props);

  return (
    <div class={`-loader-wrapper ${merged.loaderClass}`}>
      <div class={`-loader-rect ${merged.size} ${merged.color}`}>
        <div></div>
        <div></div>
        <div></div>
      </div>
    </div>
  );
};
