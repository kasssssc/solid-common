import { type Component } from 'solid-js';
import clsx from 'clsx';

interface ErrorBoxProps {
  message: string;
  class?: string;
}
export const ErrorBox: Component<ErrorBoxProps> = (props) => {
  return (
    <div class={clsx('-error-box', props.class)}>
      <i class="ri-error-warning-line" />
      <div class="-error-message">{ props.message }</div>
    </div>
  );
};