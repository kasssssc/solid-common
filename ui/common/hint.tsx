import { type ParentComponent } from 'solid-js';
import clsx from 'clsx';

import { Tooltip } from './tooltip';

interface HintProps {
  class?: string;
  tooltipClass?: string;
  width: string;
}
export const Hint: ParentComponent<HintProps> = (props) => {
  return (
    <div class={clsx('-hint', props.class && props.class)}>
      {/* <p class="-hint-text">?</p> */}
      <i class="-hint-text ri-question-line" />
      <Tooltip
        width={props.width}
        class={props.tooltipClass}
      >
        {props.children}
      </Tooltip>
    </div>
  );
};
