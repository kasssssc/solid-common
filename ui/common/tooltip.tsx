import clsx from 'clsx';
import { type ParentComponent } from 'solid-js';

interface TooltipProps {
  width: string;
  class?: string;
}
export const Tooltip: ParentComponent<TooltipProps> = (props) => {
  return (
    <div class={clsx('-tooltip', props.class)} style={{'width': props.width || 'auto'}}>
      { props.children }
    </div>
  );
};