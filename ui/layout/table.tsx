import { Component, Show, For } from 'solid-js';

interface TableProps {
  headers?: string[];
  data: any[][];
}
export const Table: Component<TableProps> = (props) => {
  return (
    <table class="-table">
      <Show when={props.headers}>
        <thead>
          <tr class="-table-row -table-header-row">
            <For each={props.headers}>
              {h => <th class="-table-cell -table-header-cell">{h}</th>}
            </For>
          </tr>
        </thead>
      </Show>
      <tbody>
        <For each={props.data}>
          {r => (
            <tr class="-table-row">
              <For each={r}>
                {c => <td class="-table-cell">{c ? convertAndRound(c) : '--'}</td>}
              </For>
            </tr>
          )}
        </For>
      </tbody>
    </table>
  );
};

function convertAndRound(str: string) {
  if (/^-?\d+(\.\d+)?$/.test(str)) {
    // Attempt to parse the string as a float
    const floatValue = parseFloat(str);
    // Check if it's a whole number (integer)
    if (Number.isInteger(floatValue)) {
      return Math.round(floatValue); // Round to the nearest integer
    }
    return parseFloat(floatValue.toFixed(4));
  }
  return str;
}