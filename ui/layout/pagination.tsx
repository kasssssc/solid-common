import clsx from 'clsx';
import { For, type Component } from 'solid-js';

interface PaginationProps {
  currentPage: number;
  onPageChange: (page: number) => void;
  totalPages: number;
  maxPageButtons: number;
}
export const Pagination: Component<PaginationProps> = (props) => {

  const prevPage = () => {
    if (props.currentPage <= 1) {
      return;
    }
    props.onPageChange(props.currentPage - 1);
  };

  const nextPage = () => {
    if (props.currentPage >= props.totalPages) {
      return;
    }
    props.onPageChange(props.currentPage + 1);
  };

  const displayPages = (): number[] => {
    const pages = [];
    const halfPage =  Math.floor(props.maxPageButtons / 2);
    let start = Math.max(1, props.currentPage - halfPage);
    let end = Math.min(props.totalPages, props.currentPage + halfPage);

    // Adjust the range when close to the beginning or end
    if (props.currentPage <= halfPage) {
      start = 1;
      end = Math.min(props.totalPages, props.maxPageButtons);
    } else if (props.currentPage + halfPage >= props.totalPages) {
      start = Math.max(1, props.totalPages - props.maxPageButtons + 1);
      end = props.totalPages;
    }

    for (let i = start; i <= end; i++) {
      pages.push(i);
    }

    return pages;
  };

  return (
    <div class="-pagination-container">
      <button
        class="-pagination-nav-btn"
        disabled={props.currentPage === 1}
        onclick={() => props.onPageChange(1)}
      >
        <i class="ri-arrow-left-double-line" />
      </button>
      <button
        class="-pagination-nav-btn"
        disabled={props.currentPage === 1}
        onclick={() => prevPage()}
      >
        <i class="ri-arrow-left-s-line" />
      </button>

      <For each={displayPages()}>
        { page => (
          <button
            class={clsx('-pagination-page-btn', page === props.currentPage && 'current')}
            onclick={() => props.onPageChange(page)}
          >
            { page }
          </button>
        )}
      </For>

      <button
        class="-pagination-nav-btn"
        onclick={() => nextPage()}
      >
        <i class="ri-arrow-right-s-line" />
      </button>
      <button
        class="-pagination-nav-btn"
        onclick={() => props.onPageChange(props.totalPages)}
      >
        <i class="ri-arrow-right-double-line" />
      </button>
    </div>
  );
};