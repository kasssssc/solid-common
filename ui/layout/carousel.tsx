import { type ParentComponent, createSignal, onMount, onCleanup, createEffect } from 'solid-js';
import clsx from 'clsx';
import { Defer } from '#common/util';
import { Button } from '../buttons';

interface CarouselProps {
  scrollOnWheel?: boolean;
  containerClass?: string;
  carouselClass?: string;
  nobtn?: boolean;
  currentId?: string;
  scrollingToCurrIdClass?: string;
}
export const Carousel: ParentComponent<CarouselProps> = (props) => {
  let carouselEl: HTMLDivElement | undefined;

  const [cantScrollLeft, setCantScrollLeft] = createSignal<boolean>(true);
  const [cantScrollRight, setCantScrollRight] = createSignal<boolean>(false);
  const [isScrollingToCurrId, setIsScrollingToCurrId] = createSignal<boolean>(false);

  const left = () => {
    carouselEl?.scrollTo({
      left: carouselEl.scrollLeft - carouselEl.offsetWidth,
      behavior: 'smooth',
    });
    updateScroll();
  };

  const right = () => {
    carouselEl?.scrollTo({
      left: carouselEl.scrollLeft + carouselEl.offsetWidth,
      behavior: 'smooth',
    });
    updateScroll();
  };

  const updateScroll = () => {
    if (!carouselEl) {
      return;
    }
    setCantScrollLeft(carouselEl.scrollLeft === 0);
    setCantScrollRight(carouselEl.scrollLeft >= (carouselEl.scrollWidth - carouselEl.offsetWidth) - 1);
  };

  const handleWheel = ($ev: any) => {
    if (!props.scrollOnWheel || !carouselEl) {
      return;
    }
    $ev.preventDefault();
    $ev.stopPropagation();
    if ($ev.deltaY !== 0) {
      carouselEl.scrollLeft += $ev.deltaY;
    }
  };

  const scrollItemIntoView = () => {
    setIsScrollingToCurrId(true);
    updateScroll();
    if (!carouselEl || !props.currentId) return;

    const currentItem = carouselEl.querySelector(`#${props.currentId}`) as HTMLElement;

    if (currentItem) {
      carouselEl.scrollLeft = currentItem.offsetLeft - carouselEl.offsetLeft;
      updateScroll();
    }
    setIsScrollingToCurrId(false);
  };

  // const delayedScroll = Defer(scrollItemIntoView);

  onMount(() => {
    // const observer = new MutationObserver(() => {
    //   delayedScroll();
    // });

    // if (carouselEl) {
    //   observer.observe(carouselEl, { childList: true, subtree: true });
    // }

    // delayedScroll();

    // onCleanup(() => {
    //   observer.disconnect();
    // });
    setTimeout(() => {
      scrollItemIntoView();
    }, 150);
  });

  return (
    <div class={clsx('-carousel-container', props.containerClass)}>
      {!props.nobtn &&
        <Button
          class="-carousel-btn"
          icon="ri-arrow-left-wide-fill"
          disabled={cantScrollLeft()}
          onclick={() => left()}
        />
      }
      <div
        class={clsx(
          '-carousel',
          props.carouselClass,
          isScrollingToCurrId() && props.scrollingToCurrIdClass,
        )}
        ref={carouselEl!}
        onscroll={() => updateScroll()}
        onWheel={handleWheel}
      >
        {props.children}
      </div>
      {!props.nobtn &&
        <Button
          class="-carousel-btn"
          icon="ri-arrow-right-wide-fill"
          disabled={cantScrollRight()}
          onclick={() => right()}
        />
      }
    </div>
  );
};
