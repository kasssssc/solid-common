import { Component, For, createSignal } from 'solid-js';

interface FoldableProps {
  header: Component;
  content: Component;
  initialState?: boolean;
}
export const Foldable: Component<FoldableProps> = (props) => {
  const [folded, setfolded] = createSignal<boolean>(!!props.initialState);

  return (
    <div class="-foldable-container">
      <div
        class="-foldable-header"
        data-folded={folded()}
        onclick={() => setfolded(!folded())}
      >
        <i class="-foldable-icon ri-arrow-down-s-line transition-all" classList={{'rotate-[-90deg]': folded()}} />
        <props.header />
      </div>
      <div class="-foldable-content" data-folded={folded()}>
        <props.content />
      </div>
    </div>
  );
};