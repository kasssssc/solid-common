import { Component, Show, createEffect } from 'solid-js';
import { Portal } from 'solid-js/web';
import { Transition } from 'solid-transition-group';

import { XBtn } from '../buttons';
import { Keyboard } from '../../enums';
import clsx from 'clsx';

interface ModalProps {
  open: boolean;
  close?: () => void;
  onCancel?: () => void;
  onConfirm?: () => void;
  content?: Component;
  footer?: Component;
  title?: string;
  size?: string;
  noXBtn?: boolean;
  bgClose?: boolean;
  escClose?: boolean;
  bodyScrollable?: boolean;
  customXbtn?: string
}

const setBodyElementClass = (modalOpen: boolean) => {
  if (modalOpen) {
    document.body.classList.add('modal-open');
  } else {
    document.body.classList.remove('modal-open');
  }
};

export const Modal: Component<ModalProps> = (props) => {
  const cancelAndClose = () => {
    props.onCancel?.();
    props.close?.();
  };

  const confirmAndClose = () => {
    props.onConfirm?.();
    props.close?.();
  };

  const keyboardEventListener = ($ev: KeyboardEvent) => {
    switch ($ev.key) {
    case Keyboard.Esc:
      if (props.escClose ?? true) {
        cancelAndClose();
      }
      break;
    case Keyboard.Enter:
      if (props.onConfirm) {
        confirmAndClose();
      }
      break;
    default:
      break;
    }
  };

  const onBgClick = () => {
    if (props.bgClose ?? true) {
      cancelAndClose();
    }
  };

  createEffect(() => {
    setBodyElementClass(props.open);
    if (props.open) {
      document.addEventListener('keydown', keyboardEventListener);
    } else {
      document.removeEventListener('keydown', keyboardEventListener);
    }
  });

  return (
    <Portal>
      <Transition name="fade">
        {props.open && (
          <div class="-modal-wrapper">
            <div class={`-modal-content ${props.size || 'sm md:md lg:lg xl:xl'}`}>
              {props.title && (
                <div class="-modal-header">
                  <Show when={props.title}>
                    <h2>{props.title}</h2>
                  </Show>
                  <Show when={!props.noXBtn}>
                    <div class="-x-btn-container">
                      <Show when={props.customXbtn} fallback={
                        <XBtn onclick={cancelAndClose} />
                      }>
                        <button class='-x-btn text-md' onclick={cancelAndClose}>{props.customXbtn}</button>
                      </Show>
                    </div>
                  </Show>
                </div>
              )}
              {!props.title && !props.noXBtn && (
                <div class="-x-btn-container-no-title">
                  <Show when={props.customXbtn} fallback={
                    <XBtn onclick={cancelAndClose} />
                  }>
                    <button class='-x-btn text-md' onclick={cancelAndClose}>{props.customXbtn}</button>
                  </Show>
                </div>
              )}
              {props.content && (
                <div class={clsx(
                  '-modal-body',
                  props.bodyScrollable && 'scroll-y-container',
                )}>
                  <props.content />
                </div>
              )}
              {props.footer && (
                <div class="-modal-footer">
                  <props.footer />
                </div>
              )}
            </div>
            <div
              class="-modal-backdrop"
              data-clickable={props.bgClose ?? true}
              onclick={() => onBgClick()}
            />
          </div>
        )}
      </Transition>
    </Portal>
  );
};
