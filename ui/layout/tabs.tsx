import { Component, For } from 'solid-js';

interface Tab {
  display: string;
  value: string;
}
interface TabProps {
  tabs: Tab[];
  selectedTab: string;
  disabled?: string[];
  onTabClick: ($ev: MouseEvent, tab: Tab, idx: number) => void;
}
export const Tabs: Component<TabProps> = (props) => {
  return (
    <div class="-tabs-container">
      <For each={props.tabs}>
        { (tab, idx) => (
          <button
            class="-tab-btn"
            data-active={props.selectedTab === tab.value}
            disabled={props.disabled?.includes(tab.value)}
            onclick={($ev: MouseEvent) => props.onTabClick($ev, tab, idx())}
          >
            { tab.display }
          </button>
        ) }
      </For>
    </div>
  );
};