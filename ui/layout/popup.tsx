import clsx from 'clsx';
import { createEffect, Show, type ParentComponent } from 'solid-js';

interface PopupProps {
  open: boolean;
  close: () => void;
  class?: string;
}
export const Popup: ParentComponent<PopupProps> = (props) => {
  let wrapperEl: HTMLDivElement;

  const clickOutsideListener = ($ev: MouseEvent) => {
    $ev.stopPropagation();
    $ev.stopImmediatePropagation();
    $ev.preventDefault();

    if (!wrapperEl) {
      return;
    }

    if (!wrapperEl.contains($ev.target as Node)) {
      props.close();
    }
  };

  createEffect(() => {
    if (props.open) {
      document.addEventListener('click', clickOutsideListener);
    } else {
      document.removeEventListener('click', clickOutsideListener);
    }
  });

  return (
    <Show when={props.open}>
      <div
        ref={wrapperEl!}
        class={clsx(
          '-popup',
          props.class,
        )}
      >
        { props.children }
      </div>
    </Show>
  );
};
