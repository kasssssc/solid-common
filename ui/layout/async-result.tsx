import { Component,ParentComponent, Switch, Match } from 'solid-js';

import { Loader } from '../common';

interface AsyncResultProps {
  loading: boolean;
  error: boolean;
  LoadingComponent?: Component;
  ErrorComponent?: Component;
}
export const AsyncResult: ParentComponent<AsyncResultProps> = (props) => {
  return (
    <Switch>
      <Match when={!props.loading && !props.error}>
        { props.children }
      </Match>
      <Match when={props.loading}>
        <div class="-async-loading-wrapper">
          { props.LoadingComponent ? <props.LoadingComponent /> : <Loader /> }
        </div>
      </Match>
      <Match when={props.error}>
        <div class="-async-error-wrapper">
          { props.ErrorComponent ? <props.ErrorComponent /> : 'Error!' }
        </div>
      </Match>
    </Switch>
  );
};