import { Component, For, Show, createSignal, createEffect } from 'solid-js';

import { Keyboard } from '../../enums';
import { SelectOption } from '../forms/select-utils';
import clsx from 'clsx';

interface DropdownProps {
  class?: string;
  open: boolean;
  filter: string;
  options: SelectOption[];
  isSelected: (option: any, idx: number) => void;
  onKeyDown: ($ev: KeyboardEvent, highlightedOption: SelectOption, highlightedIdx: number) => void; 
  onOptionClick: ($ev: MouseEvent, option: SelectOption, idx: number) => void;
}
export const Dropdown: Component<DropdownProps> = (props) => {

  const [filteredOptions, setFilteredOptions] = createSignal<SelectOption[]>([]);
  const [highlightedIdx, setHighlightedIdx] = createSignal<number>(-1);

  const getHighlightedOption = () => filteredOptions()?.[highlightedIdx()];
  const isHighlighted = (idx: number) => idx === highlightedIdx();

  const highlightPrev = () => {
    let prevIdx: number;
    if (highlightedIdx() === -1) {
      prevIdx = 0;
    } else {
      prevIdx = highlightedIdx() <= 0
        ? filteredOptions().length - 1
        : highlightedIdx() - 1;
    }
    setHighlightedIdx(prevIdx);
  };
  const highlightNext = () => {
    const nextIdx = highlightedIdx() !== -1
      ? (highlightedIdx() + 1) % filteredOptions().length
      : 0;
    setHighlightedIdx(nextIdx);
  };

  const dropdownKeyboardListener = ($ev: KeyboardEvent) => {
    switch ($ev.key) {
    case Keyboard.Up:
      $ev.preventDefault();
      highlightPrev();
      break;
    case Keyboard.Down:
      $ev.preventDefault();
      highlightNext();
      break;
    default:
      props.onKeyDown($ev, getHighlightedOption(), highlightedIdx());
      break;
    }
  };

  createEffect(() => {
    const regex = new RegExp(props.filter, 'i');
    setFilteredOptions(
      props.options.filter(o => regex.test(o.display))
    );
    setHighlightedIdx(-1);
  });

  createEffect(() => {
    if (props.open) {
      document.addEventListener('keydown', dropdownKeyboardListener);
    } else {
      document.removeEventListener('keydown', dropdownKeyboardListener);
    }
    setHighlightedIdx(-1);
  });

  return (
    <div
      class={clsx('-select-dropdown', props.class)}
      data-open={props.open}
      onmouseleave={() => setHighlightedIdx(-1)}
    >
      <For each={filteredOptions()}>
        { (option, idx) => (
          <div
            class="-select-option"
            data-highlighted={isHighlighted(idx())}
            data-selected={props.isSelected(option, idx())}
            onmouseenter={() => setHighlightedIdx(idx())}
            onclick={($ev) => props.onOptionClick($ev, option, idx())}
          >
            { option.display }
          </div>
        )}
      </For>
      <Show when={filteredOptions().length <= 0}>
        <div class="-select-option -placeholder">No options to display...</div>
      </Show>
    </div>
  );
};
