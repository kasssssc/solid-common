import { Component, Show, For } from 'solid-js';

interface Step {
  label: string;
  value: any;
  data?: any;
}

interface StepsProps {
  steps: Step[];
  currentStepIdx: number;
  activeStepIdx: number;
  onStepClick?: ($ev: MouseEvent, idx: number, data: any) => void;
}

enum StepState {
  Past = 'past',
  Current = 'curr',
  Future = 'future',
}

export const Steps: Component<StepsProps> = (props) => {
  const resolveStepState = (idx: number, currStep: number): StepState => {
    if (currStep < idx) return StepState.Future;
    if (currStep > idx) return StepState.Past;
    return StepState.Current;
  };

  return (
    <div class="-steps-container">
      <For each={props.steps}>
        { (step, idx) => (
          <Step
            step={step}
            idx={idx()}
            active={props.activeStepIdx === idx()}
            state={resolveStepState(idx(), props.currentStepIdx)}
            onStepClick={props.onStepClick}
          />
        )}
      </For>
    </div>
  );
};

interface StepProps {
  step: Step;
  idx: number;
  active: boolean;
  state: StepState;
  onStepClick?: ($ev: MouseEvent, idx: number, step: Step) => void;
}
const Step: Component<StepProps> = (props) => {
  return (
    <div
      class="-step"
      data-state={props.state}
      data-active={props.active}
    >
      <div
        class="-step-trigger"
        onClick={($ev) => props.onStepClick?.($ev, props.idx, props.step)}
      >
        <div class="-step-no">
          <Show
            when={props.state === StepState.Past}
            fallback={props.idx + 1}
          >
            <i class="ri-check-line" />
          </Show>
        </div>
        <div class="-step-label">
          <div class="">{ props.step.label }</div>
        </div>
      </div>
      <div class="-step-separator"></div>
    </div>
  );
};

