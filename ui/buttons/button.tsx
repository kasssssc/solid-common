import { JSX, Component, Show, splitProps } from 'solid-js';
import clsx from 'clsx';

import { Loader } from '../common';

interface BtnProps extends JSX.HTMLAttributes<HTMLButtonElement> {
  color?: string;
  size?: string;
  text?: string;
  icon?: string;
  spinner?: boolean;
  loading?: boolean;
  loadingText?: string;
  disabled?: boolean;
}

export const Button: Component<BtnProps> = (props) => {
  const [_, nativeProps] = splitProps(
    props,
    ['class', 'text', 'icon', 'size', 'color', 'spinner', 'loading', 'loadingText'],
  );

  return (
    <button
      class={clsx(
        '-btn',
        props.class,
        props.color,
        props.size,
      )}
      data-icon-only={!props.text}
      data-loading={props.loading}
      {...nativeProps}
    >
      <Show
        when={!props.loading}
        fallback={<Loader loaderClass="-btn-loader" />}
      >
        <Show when={props.icon}>
          <i class={props.icon} />
        </Show>
      </Show>
      {
        props.loading && props.loadingText
          ? props.loadingText
          : props.text
      }
      { props.children }
    </button>
  );
};
