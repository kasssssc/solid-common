import { JSX, Component, splitProps } from 'solid-js';
import clsx from 'clsx';

interface XBtnProps extends JSX.HTMLAttributes<HTMLButtonElement> {
  disabled?: boolean;
  icon?: string;
}
export const XBtn: Component<XBtnProps> = (props) => {
  const [_, nativeProps] = splitProps(
    props,
    ['class', 'icon'],
  );

  return (
    <button
      class={clsx('-x-btn', props.class)}
      {...nativeProps}
    >
      <i class={props.icon || 'ri-close-fill'} />
    </button>
  );
};
